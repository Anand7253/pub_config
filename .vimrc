set nocompatible
set number
set tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab
set foldmethod=indent foldlevelstart=99 foldnestmax=1
set splitright splitbelow
set nostartofline
nohlsearch
map <F2> <Cmd>exec 'set scrolloff='..v:count<CR>
let g:ruby_recommended_style = 0

if &term == 'linux'
	highlight Comment   cterm=bold ctermfg=0
	highlight Delimiter ctermfg=1
	highlight LineNr    cterm=none
	highlight Special   cterm=bold ctermfg=4
	highlight Statement cterm=none
else
	highlight Comment             ctermfg=243
	highlight Delimiter           ctermfg=196
	highlight Folded              ctermbg=0
	highlight LineNr    term=NONE ctermfg=178 gui=NONE
	highlight Special             ctermfg=63
	highlight Statement term=NONE ctermfg=178 gui=NONE
	highlight PerlStringStartEnd  ctermfg=196
	highlight rubyRegexpCharClass ctermfg=34
endif
